// BGnomeImport.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BGnomeImporter.h"

int _tmain(int argc, _TCHAR* argv[])
{
	BGnomeImporter* import = new BGnomeImporter;
	std::string m_open;
	int m_wait = 1;

	std::vector<float> goMesh;
	while(m_wait)
	{
		std::cout << "filePath?" << std::endl;
		std::cin >> m_open;
		//for (int i = 0 ; i < 10000; i++)
			m_wait = import->Go(m_open, goMesh);

		std::cout << "done!" << std::endl;
	};
	delete import;
	std::cin >> m_wait;
	return 0;
}


#include "StdAfx.h"
#include "BGnomeImporter.h"



BGnomeImporter::BGnomeImporter(void)
{	
	std::cout << "Importer is initilized" << std::endl;
}

/*
The function Go will get you a mesh for now.

the function will add it to the float vector you've sent in, pushing it back

it is sorted by

position - normal - uv- tangent - binormal

You also need to send in a path for the file, as a string

It used fstream and ascii, so the function will be optimized in the future :)

I'll be here if you need to ask something :)

//Alice



*/
int BGnomeImporter::Go(std::string filePath, std::vector<float> &getMesh)
{

	//std::cout << "reading file" << std::endl;

	std::fstream m_file;
	std::fstream m_animFile;
	m_file.open(filePath, std::ios::in | std::ios::binary);
	std::cout << filePath << std::endl;
	if (m_file)
	{
		ReadFile(m_file);
		m_file.close();
		std::cout << "bgnome read" << std::endl;
		m_animFile.open(GetFileNameAndPath(filePath,".") + ".bagnome", std::ios::in | std::ios::binary);
		std::cout << GetFileNameAndPath(filePath,".") + ".bagnome" << std::endl;
		if (m_animFile)
		{
			ReadFile(m_animFile);
		}
		m_animFile.close();
		std::cout << "bagnome read" << std::endl;
		deletetion();
		return 0;
	}
	m_file.close();
	std::cout << "ERROR" << std::endl;
	return 1;
}

int BGnomeImporter::ReadFile(std::fstream &file)
{
	if(checkByte(file, "GNOME", 6))
	{
		HeaderFile(file, (void*)&header, sizeof(header)); // done
		std::cout << header.numberOfTriangles << " " << header.numberOfVertices << " " << header.numberOfIndices << std::endl;
		mesh.vertices	= new Vertex[header.numberOfVertices]; 
		mesh.animatedVertices = new VertexBoneAnimated[header.numberOfVertices];
		mesh.indices	= new int[header.numberOfIndices];
		bones			= new Bone[header.numberOfBones];
		VertexFile(file); // done
		IndexFile(file); // done
		if (header.numberOfBones)
			BoneFile(file); //done
		return 0;
	}
	else if(checkByte(file, "ANIMGNOME", 10))
	{
		HeaderFile(file, (void*)&headAnim, sizeof(headAnim)); // done
		//bones			= new Bone[headAnim.numberOfBones];
		animations		= new Animation[headAnim.numberOfAnimations];
		morphAnimation	= new MorphAnimation[headAnim.numberOfAnimations]; 

		if (headAnim.numberOfBones == header.numberOfBones && header.numberOfBones)
		{
			//BoneFile(file); //done
			if (headAnim.numberOfAnimations)
				AnimationFile(file); //done
		}
		else if (headAnim.numberOfAnimations)
		{
			MorphFile(file); // done
		}
		return 0;
	}
	else
	{
		std::cout << "ERROR: File is not a .BGNOME nor a .BAGNOME, of a obsolete version of .BGNOME or corrupted" << std::endl; 
		return 1;
	}
}

std::string BGnomeImporter::GetFileNameAndPath(std::string filename, std::string delim)
{
	std::string gFile;

	size_t pos = filename.find_last_of(delim);

	for (unsigned int i = 0; i < pos; i++)
	{
		gFile.push_back(filename[i]);
	}
    
	return gFile;
}

int BGnomeImporter::checkByte(std::fstream &file, char* mb, int byteSize)
{
	file.seekg(0, std::ios::beg);
	char* magicByte = new char[byteSize];
	file.read((char*) magicByte, byteSize);

	
	if(strcmp(magicByte, mb) == 0)
	{
		delete [] magicByte;
		return 1;
	}
	else
	{
		std::cout << magicByte << std::endl;
		delete [] magicByte;
		return 0;
	}
}

int BGnomeImporter::IndexFile(std::fstream &file)
{
	file.read((char*)mesh.indices, sizeof(int) * header.numberOfIndices);
	return 1;
}

int BGnomeImporter::MorphFile(std::fstream &file)
{
	for (int i = 0; i < headAnim.numberOfAnimations; i++)
	{
		file.read((char*)&morphAnimation[i].id, sizeof(int));
		file.read((char*)&morphAnimation[i].numberOfKeys, sizeof(int));
		morphAnimation[i].keys = new MorphKey[morphAnimation[i].numberOfKeys];

		for (int j = 0; j < morphAnimation[i].numberOfKeys; j++)
		{
			morphAnimation[i].keys[j].vertices = new Vertex[header.numberOfVertices];
			file.read((char*)&morphAnimation[i].keys[j].keyTime, sizeof(int));
			file.read((char*)&morphAnimation[i].keys[j].vertices, sizeof(Vertex) * header.numberOfVertices);
		}

	}
	return 1;
}
int BGnomeImporter::VertexFile(std::fstream &file)
{
	//std::cout << "Vertices " << header.numberOfVertices << " " << header.numberOfTriangles << std::endl;

	if (header.numberOfBones)
		file.read((char*)mesh.animatedVertices, sizeof(VertexBoneAnimated)*header.numberOfVertices);
	else
		file.read((char*)mesh.vertices, sizeof(Vertex) * header.numberOfVertices);


	return 1;
}

int BGnomeImporter::HeaderFile(std::fstream &file, void* header, int size)
{
	file.read((char*)header, size);
	return 1;
}

int BGnomeImporter::BoneFile(std::fstream &file)
{
	std::cout << header.numberOfBones << std::endl;
	for (int k = 0; k < header.numberOfBones; k++)
	{
		
		file.read((char*)&bones[k], sizeof(int)*3 + sizeof(float)*4*4);
		char *tmp = new char[bones[k].nameSize];
		file.read((char*)tmp, bones[k].nameSize);
		bones[k].name = tmp;
		delete [] tmp;
		std::cout << bones[k].name << std::endl;
	}

	//for (int i = 0; i < 4; i++)
	//{
	//	for (int j = 0; j < 4; j++)
	//		std::cout << bones[0].offsetMatrix[j][i] << " ";
	//	std::cout << std::endl;
	//}

	return 1;
}

int BGnomeImporter::AnimationFile(std::fstream &file)
{
	for (int i = 0; i < headAnim.numberOfAnimations; ++i)
	{
		file.read((char*)&animations[i].nameSize, sizeof(int));
		char* tmp = new char[animations[i].nameSize];
		file.read((char*)tmp, animations[i].nameSize);
		animations[i].name = std::string(tmp);
		std::cout << "RUN YOU FOOLS! "<< animations[i].name << " " << animations[i].nameSize << std::endl;
		delete [] tmp;
		animations[i].boneAnim = new BoneForAnimation[headAnim.numberOfBones];

		for (int j = 0; j < headAnim.numberOfBones; ++j)
		{	
			file.read((char*)&animations[i].boneAnim[j].numKeys, sizeof(int));
			animations[i].boneAnim[j].Keyframes = new Keyframe[animations[i].boneAnim[j].numKeys];
			file.read((char*)animations[i].boneAnim[j].Keyframes, sizeof(Keyframe) * animations[i].boneAnim[j].numKeys); 
			std::cout << animations[i].boneAnim[j].numKeys << std::endl;
		}
	}
	return 1;
}

void BGnomeImporter::deletetion(void)
{
	if (header.numberOfBones > 0)
		for (int i = 0; i < headAnim.numberOfAnimations; ++i)
		{
			for (int j = 0; j < header.numberOfBones; ++j)
				delete [] animations[i].boneAnim[j].Keyframes;
			delete [] animations[i].boneAnim;
		}
	else if (headAnim.numberOfAnimations > 0)
		for (int i = 0; i < headAnim.numberOfAnimations; i++)
		{
			for (int j = 0; j < morphAnimation[i].numberOfKeys; j++)
				delete [] morphAnimation[i].keys[j].vertices;
			delete [] morphAnimation[i].keys;
		}

	delete [] mesh.vertices;
	delete [] mesh.indices;
	delete [] mesh.animatedVertices;
	delete [] bones;
	//delete [] animations;
	//delete [] morphAnimation;
}

BGnomeImporter::~BGnomeImporter(void)
{

}

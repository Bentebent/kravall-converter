#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>

class BGnomeImporter
{
public:
	struct Header
	{
		int numberOfVertices;
		int numberOfTriangles;
		int numberOfIndices;
		int numberOfBones;
	};

	struct HeaderAnim
	{
		int numberOfBones;
		int numberOfAnimations;
	};

	struct Bone
	{
		int id;
		int parentID;
		int nameSize;
		float offsetMatrix[4][4];
		std::string name;
	};

	struct Keyframe
	{
		float time;
		float position[3];
		float scale[3];
		float rotation[4];
	};
	
	struct BoneForAnimation
	{
		int numKeys;
		Keyframe* Keyframes;
	};

	struct Animation
	{
			//int fps;
			//int id;
			int nameSize;
			std::string name;
			BoneForAnimation* boneAnim;
	};


	Animation* animations;

	struct VertexBoneAnimated
	{
		//Ordinary stuff
		float position[4];
		float normal[4];

		//Normalmapping
		float tangent[4];

		//Animation
		int boneIndex[4];
		float boneWeight[4];
			
		//uv
		float uv[2];
	};

	struct Vertex
	{
		//Ordinary stuff
		float position[3];
		float normal[3];
		
		//Normalmapping
		float tangent[3];
		float uv[2];
	};

	struct Mesh
	{
		int* indices;
		Vertex* vertices;
		VertexBoneAnimated* animatedVertices;
	};

	struct MorphKey
	{
		float keyTime;
		Vertex* vertices;
	};

	struct MorphAnimation
	{
		int id;
		int numberOfKeys;
		MorphKey* keys;
	};

	

	Bone* bones;

	Header header;
	HeaderAnim headAnim;
	Mesh mesh;
	MorphAnimation* morphAnimation;
	
	int Go(std::string filePath, std::vector<float> &mesh);
	int Go(std::string filePath, char* meshPointer);
	int ReadFile(std::fstream &file);

	int checkByte(std::fstream &file, char* magicByte, int byteSize);
	int HeaderFile(std::fstream &file, void* header, int size);
	int VertexFile(std::fstream &file);
	int BoneFile(std::fstream &file);
	int AnimationFile(std::fstream &file);
	int MorphFile(std::fstream & file);
	int IndexFile(std::fstream &file);

	std::string GetFileNameAndPath(std::string filename, std::string delim);

	void deletetion(void);

	BGnomeImporter(void);
	~BGnomeImporter(void);


};


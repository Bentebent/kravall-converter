#include "StdAfx.h"
#include "BGnomeImporterAscii.h"

BGnomeImporterAscii::Header header;
BGnomeImporterAscii::Mesh mesh;
BGnomeImporterAscii::MorphAnimation* morphAnimation;

BGnomeImporterAscii::BGnomeImporterAscii(void)
{	
	std::cout << "Importer is initilized" << std::endl;
}

/*
The function Go will get you a mesh for now.

the function will add it to the float vector you've sent in, pushing it back

it is sorted by

position - normal - uv- tangent - binormal

You also need to send in a path for the file, as a string

It used fstream and ascii, so the function will be optimized in the future :)

I'll be here if you need to ask something :)

//Alice

*/
int BGnomeImporterAscii::GoASCII(std::string filePath, std::vector<float> &getMesh)
{

	std::cout << "reading file" << std::endl;

	std::fstream m_file;
	
	m_file.open(filePath, std::ios::in);
	if (m_file)
	{
		ReadASCII(m_file);
		m_file.close();

		for (int i = 0; i < header.numberOfVertices; i++)
		{
			for( int j = 0; j < 3; j++)
				getMesh.push_back(mesh.vertices[i].position[j]);
			for( int j = 0; j < 3; j++)
				getMesh.push_back(mesh.vertices[i].normal[j]);
			for( int j = 0; j < 2; j++)
				getMesh.push_back(mesh.vertices[i].uv[j]);
			for( int j = 0; j < 3; j++)
				getMesh.push_back(mesh.vertices[i].tangent[j]);
			for( int j = 0; j < 3; j++)
				getMesh.push_back(mesh.vertices[i].binormal[j]);
		}

		return 0;

	}
	m_file.close();
	std::cout << "ERROR" << std::endl;
	return 1;
}

int BGnomeImporterAscii::ReadASCII(std::fstream &file)
{
	
	std::string m_line;
	HeaderASCII(file);
	//mesh.materials	= new Material[header.numberOfMaterials];
	mesh.vertices	= new Vertex[header.numberOfVertices]; 
	mesh.indices	= new int[header.numberOfIndices];
	bones			= new Bone[header.numberOfBones];
	if (header.numberOfBones && header.numberOfAnimations)
		animations		= new Animation[header.numberOfAnimations];
	else if (header.numberOfAnimations)
		morphAnimation	= new MorphAnimation[header.numberOfAnimations]; 
	//MaterialASCII(file);
	VertexASCII(file);
	IndexASCII(file);

	if (header.numberOfBones)
	{
		BoneASCII(file);
		if (header.numberOfAnimations)
			AnimationASCII(file);
	}
	else if (header.numberOfAnimations)
	{
		MorphASCII(file);
	}

	return 0;
}
int BGnomeImporterAscii::IndexASCII(std::fstream &file)
{
	std::string m_line;
	file >> m_line;
	for (int i = 0; i < header.numberOfIndices; i++)
	{
		file >> mesh.indices[i];
		std::cout << mesh.indices[i] << std::endl;
	}
	return 0;
}
int BGnomeImporterAscii::MorphASCII(std::fstream &file)
{
	std::string m_line;
	file >> m_line;

	for (int i = 0; i < header.numberOfAnimations; i++)
	{
		file >> morphAnimation[i].name;
		file >> m_line;
		file >> morphAnimation[i].numberOfKeys;
		morphAnimation[i].keys = new MorphKey[morphAnimation[i].numberOfKeys];
		file >> m_line;
		for (int j = 0; j < morphAnimation[i].numberOfKeys; j++)
		{
			file >> m_line;
			file >> morphAnimation[i].keys[j].keyTime;
			file >> m_line;

			morphAnimation[i].keys[j].vertices = new Vertex[header.numberOfVertices];
			for (int k = 0; k < header.numberOfVertices; k++)
			{
				
				file >> m_line;
				file >> morphAnimation[i].keys[j].vertices[k].position[0]	 >> morphAnimation[i].keys[j].vertices[k].position[1]	 >> morphAnimation[i].keys[j].vertices[k].position[2];
				file >> m_line;
				file >> morphAnimation[i].keys[j].vertices[k].normal[0]		 >> morphAnimation[i].keys[j].vertices[k].normal[1]		 >> morphAnimation[i].keys[j].vertices[k].normal[2];
				file >> m_line;
				file >> morphAnimation[i].keys[j].vertices[k].tangent[0]	 >> morphAnimation[i].keys[j].vertices[k].tangent[1]	 >> morphAnimation[i].keys[j].vertices[k].tangent[2];
				file >> m_line;
				file >> morphAnimation[i].keys[j].vertices[k].binormal[0]	 >> morphAnimation[i].keys[j].vertices[k].binormal[1]	 >> morphAnimation[i].keys[j].vertices[k].binormal[2];
				file >> m_line;
				file >> morphAnimation[i].keys[j].vertices[k].uv[0]			 >> morphAnimation[i].keys[j].vertices[k].uv[1];
			}

			file >> m_line;
		}
		file >> m_line;

	}

	//std::cout << morphAnimation[0].keys[0].vertices[1].position[0] << std::endl;
	return 0;
}
int BGnomeImporterAscii::VertexASCII(std::fstream &file)
{
	std::cout << "Vertices " << header.numberOfVertices << " " << header.numberOfTriangles << std::endl;
	std::string m_line;
	//int m_materialId;

	Vertex vertex;

	file >> m_line;
	//file >> m_line >> m_materialId; //todo: make dynamic solution for mutiple materials
	//vertex.materialId = m_materialId;
	for (int i = 0; i < header.numberOfVertices; ++i)
	{
		file >> m_line;
		file >> vertex.position[0]	 >> vertex.position[1]	 >> vertex.position[2];
		file >> m_line;
		file >> vertex.normal[0]	 >> vertex.normal[1]	 >> vertex.normal[2];
		file >> m_line;
		file >> vertex.tangent[0]	 >> vertex.tangent[1]	 >> vertex.tangent[2];
		file >> m_line;
		file >> vertex.binormal[0]	 >> vertex.binormal[1]	 >> vertex.binormal[2];
		file >> m_line;
		file >> vertex.uv[0]		 >> vertex.uv[1];
		if (header.numberOfBones) //V�FFLAN
		{
			file >> m_line;
			file >> vertex.boneWeight[0] >> vertex.boneWeight[1] >> vertex.boneWeight[2] >> vertex.boneWeight[3];
			file >> m_line;
			file >> vertex.boneIndex[0]	 >> vertex.boneIndex[1]	 >> vertex.boneIndex[2]	 >> vertex.boneIndex[3];
		}

		mesh.vertices[i] = vertex;
	}
	return 0;
}

//int BGnomeImporterAscii::MaterialASCII(std::fstream & file)
//{
//	std::cout << "Materials " << header.numberOfMaterials << std::endl;
//	std::string m_line;
//
//	Material material;	
//
//	file >> m_line;
//	for (int i = 0; i < header.numberOfMaterials; ++i)
//	{
//		file >> material.name;
//		file >> m_line;
//		file >> material.ambient[0]		>> material.ambient[1]		>> material.ambient[2];
//		file >> m_line;
//		file >> material.diffuse[0]		>> material.diffuse[1]		>> material.diffuse[2];
//		file >> m_line;
//		file >> material.specularity[0] >> material.specularity[1]	>> material.specularity[2];
//		file >> m_line;
//		file >> material.specularityPower;
//		file >> m_line;
//		file >> material.reflectivity;
//		file >> m_line;
//		file >> material.transparency;
//		file >> m_line;
//		file >> material.alphaClip;
//		file >> m_line;
//		file >> material.diffuseTexture;
//		file >> m_line;
//		file >> material.normalMap;
//		file >> m_line;
//		file >> material.alphaMap;
//
//		mesh.materials[i] = material;
//		std::cout << mesh.materials[i].name << std::endl;
//		std::cout << mesh.materials[i].diffuseTexture << std::endl;
//	}
//
//	return 0;
//}

int BGnomeImporterAscii::HeaderASCII(std::fstream &file)
{
	std::string m_line;
	file >> m_line;

	//file >> m_line;
	//file >> header.numberOfMaterials;
	file >> m_line;
	file >> header.numberOfVertices;
	file >> m_line;
	file >> header.numberOfTriangles;
	file >> m_line;
	file >> header.numberOfIndices;
	file >> m_line;
	file >> header.numberOfBones;
	file >> m_line;
	file >> header.numberOfAnimations;
	
	
	
	//std::cout << header.numberOfMaterials << std::endl;
	std::cout << header.numberOfVertices << std::endl;
	std::cout << header.numberOfTriangles << std::endl;
	std::cout << header.numberOfIndices << std::endl;
	std::cout << header.numberOfBones << std::endl;
	std::cout << header.numberOfAnimations << std::endl;

	return 0;
}

int BGnomeImporterAscii::BoneASCII(std::fstream &file)
{
	std::string m_line;
	file >> m_line;

	Bone bone;

	for (int k = 0; k < header.numberOfBones; k++)
	{
		file >> bone.Name;
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
			{
				file >> bone.offsetMatrix[i][j];
				if (k == 1)
					std::cout << bone.offsetMatrix[i][j] << " ";
			}
		bones[k] = bone;
		if (k == 1)
			std::cout << std::endl;
	}
	
	file >> m_line;
	for (int i = 0; i < header.numberOfBones; i++)
	{
		file >> m_line;
		file >> bones[i].parentID;
		bones[i].id = i;
	}
	return 1;
}

int BGnomeImporterAscii::AnimationASCII(std::fstream &file)
{
	std::string m_line;
	file >> m_line;
	file >> m_line;
	Animation animation;
	

	for (int i = 0; i < header.numberOfAnimations; ++i)
	{
		file >> m_line;
		file >> animation.name;
		file >> m_line;

		for (int j = 0; j < header.numberOfBones; ++j)
		{
			int m_noKeys;
			file >> m_line >> m_line >> m_noKeys;
			file >> m_line;
			
			std::vector<Keyframe> keys; //TODO: unvectorize //c:\users\alice\downloads\flag.gnome
			for (int k = 0; k < m_noKeys; k++)
			{
				Keyframe keyframe;
				file >> m_line;
				file >> keyframe.time;
				file >> m_line;
				file >> keyframe.position[0] >> keyframe.position[1] >> keyframe.position[2];
				file >> m_line;
				file >> keyframe.scale[0]	 >> keyframe.scale[1]	 >> keyframe.scale[2]; 
				file >> m_line;
				file >> keyframe.rotation[0] >> keyframe.rotation[1] >> keyframe.rotation[2] >> keyframe.rotation[3];


				keys.push_back(keyframe);
			}
			file >> m_line;

			animations[i].keyframes.push_back(keys);
			animations[i].namedKeyframes[bones[j].Name] = keys;
		}
	}

	return 1;
}

BGnomeImporterAscii::~BGnomeImporterAscii(void)
{

}

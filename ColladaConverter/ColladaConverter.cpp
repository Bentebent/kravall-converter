#include "ColladaConverter.h"
#include "DirectoryHandler.h"

ColladaConverter::ColladaConverter()
{
}

ColladaConverter::~ColladaConverter()
{
}

int ColladaConverter::Convert(bool batchConvert, std::string file ) 
{
    return Convert( batchConvert, file, "" );
}

int ColladaConverter::Convert(bool batchConvert, std::string file, std::string gFile ) 
{
	std::string trash;
	std::string filename = std::string(file);
	std::vector<std::string> files;
	std::vector<std::string> animationNames;
	Assimp::Importer imp;
	Header head;

	std::vector <Face> faceArray;
	std::vector <Vertex> vertexArray;
	std::vector <Bone> boneArray;

    std::string get_mesh_index =   std::string("y");
    std::string export_as_binary = std::string("y");

    bool prompt = true;

    if(batchConvert || gFile != "" )
    {
        prompt = false;
        std::cout << "Disabling prompting due to command line input" << std::endl;
    }
    else
    {
        std::cout << "Enabling prompting due to command line input" << std::endl;
    }

	GetFilesFromMetaFile(files, animationNames, filename.c_str());

	std::string fileDirectory, delim;
	delim = std::string(".");
	
    if( gFile == "" )
    { 
	    gFile = GetFileNameAndPath(filename, delim);
    }

    delim = std::string("/");
    fileDirectory = GetFileNameAndPath(filename, delim);
    fileDirectory += "/";
    




    if( files.size() > 0 )
    {
        const aiScene* scene = imp.ReadFile((fileDirectory + files[0]), aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials | aiProcess_JoinIdenticalVertices | aiProcess_FlipUVs | aiProcess_CalcTangentSpace | aiProcess_LimitBoneWeights);

        

        /*
        aiProcess_CalcTangentSpace
        aiProcess_JoinIdenticalVertices
            aiProcess_MakeLeftHanded
        aiProcess_Triangulate
            aiProcess_RemoveComponent
            aiProcess_GenNormals
            aiProcess_GenSmoothNormals
            aiProcess_SplitLargeMeshes
            aiProcess_PreTransformVertices
            aiProcess_LimitBoneWeights
            aiProcess_ValidateDataStructure
            aiProcess_ImproveCacheLocality
        aiProcess_RemoveRedundantMaterials
            aiProcess_FixInfacingNormals
            aiProcess_SortByPType
            aiProcess_FindDegenerates
            aiProcess_FindInvalidData
        aiProcess_GenUVCoords
            aiProcess_TransformUVCoords
            aiProcess_FindInstances
            aiProcess_OptimizeMeshes
            aiProcess_OptimizeGraph
        aiProcess_FlipUVs
            aiProcess_FlipWindingOrder
            aiProcess_SplitByBoneCount
            aiProcess_Debone
        */

        if (!scene)
        {
            std::cout << "FATAL: " <<  imp.GetErrorString() << std::endl;
            return 1;
        }



        std::cout << "File found" << std::endl << std::endl;

        if( scene->mNumMeshes != 1 )
        {
            std::cerr << "FATAL: MORE THAN ONE MESH IN OBJECT, THIS IS PROBABLY NOT INTENDED" << std::endl;
            return 10;
        }

        aiMesh *mesh = scene->mMeshes[0]; //Assuming you only want the first mesh. In this project we only went one mesh
        
        
        head.numVertices = 0;
        head.numTriangles = 0;
        head.numIndices = 0;
        head.numBones = 0;
        head.numAnimations = 0;
        unsigned int numAffectingBones = mesh->mNumBones;

        if( prompt )
        {
            std::cout << "Get mesh as indexed? (y/n)" << std::endl;
            std::cin >> get_mesh_index;
        }
        
        if((strcmp(get_mesh_index.c_str(), "y") == 0))
        {
            if(!GetIndexedMesh(vertexArray, faceArray, mesh, head))
            {
                std::cout << "Get mesh data failed" << std::endl;
                if( prompt )
                    std::cin >> trash;
                return 1;
            }
            if( prompt )
            std::cout << "Mesh loaded" << std::endl << std::endl;
        }
        else
        {
            if(!GetMesh(vertexArray, mesh, head))
            {
                std::cout << "Get mesh data failed" << std::endl;
                if( prompt )
                    std::cin >> trash;
                return 1;
            }
            std::cout << "Mesh loaded" << std::endl << std::endl;
        }

        if (numAffectingBones)
        {
            aiNode* rootNode = scene->mRootNode;
            if (!GetBones(boneArray, rootNode, vertexArray, mesh, head))
            {
                std::cout << "Get bone data failed" << std::endl;

                std::cout << "Bones and bone animations will be ignored" << std::endl << std::endl;

                // Set the vertex weights that did not get any weights to zero
                SetZeroWeights(vertexArray);
            }
            else
            {
                //std::cout << "Bones loaded" << std::endl << std::endl;
                //aiNode* rootNode = scene->mRootNode;
                //if(!GetBoneHierarchy(boneArray, rootNode))
                //{
                //	std::cout << "Get bone hierarchy failed" << std::endl;
                //    if( prompt )
                //	    std::cin >> trash;
                //	return 1;
                //}	
                //std::cout << "Bone hierarchy loaded" << std::endl << std::endl;



                for (unsigned int i = 1; i < files.size(); i++)
                {
                    Assimp::Importer tmpAnimImporter;
                    const aiScene* tmpAnimScene = tmpAnimImporter.ReadFile((fileDirectory + files[i]), aiProcess_Triangulate | aiProcess_RemoveRedundantMaterials | aiProcess_JoinIdenticalVertices | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

                    if (!tmpAnimScene)
                    {
                        std::cout << ("Error opening animation scene '%s': '%s'\n", (fileDirectory + files[i]).c_str(), tmpAnimImporter.GetErrorString()) << std::endl;
                        if (prompt)
                            std::cin >> trash;
                        return 1;
                    }

                    unsigned int numAnimations = tmpAnimScene->mNumAnimations;
					if (numAnimations > 1)
					{
						std::cout << "There are more than one animations in this file. Converter only support one animation in one file. ABORTING!! " << (fileDirectory + files[i]) << std::endl << std::endl;
						if (prompt)
							std::cin >> trash;
						return 1;
					}

                    if (numAnimations)
                    {
                        std::cout << "File #" << i << std::endl << std::endl;
                        head.numAnimations += numAnimations;
                        aiAnimation** animations = tmpAnimScene->mAnimations;
						if (!GetBoneAnimation(boneArray, animations, numAnimations, animationNames[i - 1]))
                        {
                            std::cout << "Get animations failed" << std::endl << std::endl;
                            if (prompt)
                                std::cin >> trash;
                            return 1;
                        }
                    }
                    else
                    {
                        std::cout << "There are no animations in file: " << (fileDirectory + files[i]) << std::endl << std::endl;
                    }
                }
                std::vector <Bone> tmpBoneArray(boneArray);



                /*
                for(unsigned int i = 0; i < boneArray.size(); i++)
                {
                for(unsigned int k = 0; k < boneArray[i].anim.size(); k++)
                {
                for(unsigned int l = 0; l < boneArray[i].anim[k].frame.size(); l++)
                {
                boneArray[i].anim[k].frame[l].pos += GetWorldTransformationAnimationPosition(tmpBoneArray, tmpBoneArray[i].parentID, k, l);

                aiVector3D scale = GetWorldTransformationAnimationScale(tmpBoneArray, tmpBoneArray[i].parentID, k, l);
                boneArray[i].anim[k].frame[l].scale.x *= scale.x;
                boneArray[i].anim[k].frame[l].scale.y *= scale.y;
                boneArray[i].anim[k].frame[l].scale.z *= scale.z;

                boneArray[i].anim[k].frame[l].quat = GetWorldTransformationAnimationRotation(tmpBoneArray, tmpBoneArray[i].parentID, k, l) * boneArray[i].anim[k].frame[l].quat;
                boneArray[i].anim[k].frame[l].quat.Normalize();
                }
                }
                std::cout << "Percent of animation conversion done: " << ((float)i / (float)boneArray.size()) * 100 << "%" << std::endl;
                }

                */



                for (unsigned int i = 0; i < boneArray.size(); i++)
                {
                    for (unsigned int k = 0; k < boneArray[i].anim.size(); k++)
                    {
                        if (!boneArray[i].anim[k].frame.size())
                        {
                            Frame key;
                            aiNode* bone = rootNode->FindNode(boneArray[i].name);

                            aiMatrix4x4 transMatrix = bone->mTransformation;
                            transMatrix.Decompose(key.scale, key.quat, key.pos);

                            key.time = 0.0f;
                            boneArray[i].anim[k].frame.push_back(key);
                            key.time = 0.0001f;
                            boneArray[i].anim[k].frame.push_back(key);
                        }
                    }
                }




                std::cout << "Bone animation loaded" << std::endl << std::endl;
            }
        }
        else
        {
            SetZeroWeights(vertexArray);
            std::cout << "There is no bones..." << std::endl << std::endl;
        }

        if( prompt )
        {
            std::cout << "Export as binary? (y/n)" << std::endl;
            std::cin >> export_as_binary;
        }
        
        if((strcmp(export_as_binary.c_str(), "y") == 0))
        {
            if (exportFileBinary(vertexArray, faceArray, head, boneArray, numAffectingBones, gFile))
            {
                std::cout << "Export succesful" << std::endl;
            }
            else
            {
                std::cout << "Export failed" << std::endl;
            }
        }
        else
        {
            if (exportFileAscii(vertexArray, faceArray, head, boneArray, numAffectingBones, gFile))
            {
                std::cout << "Export succesful" << std::endl;
            }
            else
            {
                std::cout << "Export failed" << std::endl;
            }
        }
        if( prompt )
            std::cin >> trash;


        
        //aiReleaseImport(scene);

        return 0;
    }
    else
    {
        std::cerr << "Something fucked up, no files" << std::endl;
        return 3;
    }
}

std::string ColladaConverter::GetFileNameAndPath(std::string filename, std::string delim)
{
	std::string gFile;

	size_t pos = filename.find_last_of(delim);

    if( pos == filename.npos )
        pos = 0;

	for (unsigned int i = 0; i < pos; i++)
	{
		gFile.push_back(filename[i]);
	}
    
	return gFile;
}


bool ColladaConverter::GetFilesFromMetaFile(std::vector<std::string> &files, std::vector<std::string> &animationNames, const char* filePath)
{
	std::fstream file(filePath, std::ios::in);
	if (!file)
	{
		return false;
	}
	if (!file.eof())
	{
		std::string line;
		std::getline(file, line);
		files.push_back(line);
	}

	while (!file.eof())
	{
		std::string line;
		std::getline(file, line);
		std::cout << "Adding: " << line << std::endl;
		files.push_back(line);


		std::getline(file, line);
		animationNames.push_back(line);
	}

	file.close();

	return true;
}

//Calculates the tangent and binormal for a faces verticis
void ColladaConverter::CalculateTangent(
	aiVector3D P0,
	aiVector3D P1,
	aiVector3D P2,
	aiVector3D UV0,
	aiVector3D UV1,
	aiVector3D UV2,
	aiVector3D &tangent,
	aiVector3D &binormal
	)
{
	//Calculate tangent code taken from:
	//http://www.gamedev.net/topic/571707-how-to-calculate-tangent-binormal-normal-vectors-thanks/

	aiVector3D e0 = P1 - P0;
	aiVector3D e1 = P2 - P0;
	
	//using Eric Lengyel's approach with a few modifications
	//from Mathematics for 3D Game Programmming and Computer Graphics
	// want to be able to trasform a vector in Object Space to Tangent Space
	// such that the x-axis cooresponds to the 's' direction and the
	// y-axis corresponds to the 't' direction, and the z-axis corresponds
	// to <0,0,1>, straight up out of the texture map

	//let P = v1 - v0
	aiVector3D P = P1 - P0;
	//let Q = v2 - v0
	aiVector3D Q = P2 - P0;
	float s1 = UV1.x - UV0.x;
	float t1 = UV1.y - UV0.y;
	float s2 = UV2.x - UV0.x;
	float t2 = UV2.y - UV0.y;


	//we need to solve the equation
	// P = s1*T + t1*B
	// Q = s2*T + t2*B
	// for T and B


	//this is a linear system with six unknowns and six equatinos, for TxTyTz BxByBz
	//[px,py,pz] = [s1,t1] * [Tx,Ty,Tz]
	// qx,qy,qz     s2,t2     Bx,By,Bz

	//multiplying both sides by the inverse of the s,t matrix gives
	//[Tx,Ty,Tz] = 1/(s1t2-s2t1) *  [t2,-t1] * [px,py,pz]
	// Bx,By,Bz                      -s2,s1	    qx,qy,qz  

	//solve this for the unormalized T and B to get from tangent to object space

	float tmp = 0.0f;
	if(fabsf(s1 * t2 - s2 * t1) <= 0.0001f)
	{
		tmp = 1.0f;
	}
	else
	{
		tmp = 1.0f/(s1*t2 - s2*t1);
	}

	tangent.x = (t2 * P.x - t1 * Q.x);
	tangent.y = (t2 * P.y - t1 * Q.y);
	tangent.z = (t2 * P.z - t1 * Q.z);

	tangent = tangent * tmp;

	binormal.x = (s1 * Q.x - s2 * P.x);
	binormal.y = (s1 * Q.y - s2 * P.y);
	binormal.z = (s1 * Q.z - s2 * P.z);

	binormal = binormal * tmp;

	tangent.Normalize();
	binormal.Normalize();
}

//Gets the mesh from Assimp
bool ColladaConverter::GetMesh(std::vector<Vertex>& vertexArray, aiMesh* mesh, Header& head)
{
	std::string trash;
	
	bool hasTangents = mesh->HasTangentsAndBitangents();
	aiVector3D* binormals;
	aiVector3D* tangents;
	if(hasTangents)
	{
		binormals = mesh->mBitangents;
		tangents = mesh->mTangents;
	}
	else
	{
		std::cout << "The mesh DON'T have binormals and tangents. Tangents and binormals will be calculated instead. Contact your nearest Technichal Artist or Johan to fix this.  :P" << std::endl << std::endl;
	}

	bool continueRead = false;
	for(unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		const aiFace& face = mesh->mFaces[i];

		if(continueRead == false)
		{
			if(face.mNumIndices > 3)
			{
				std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
				std::cout << "THIS FACE HAS MORE THAN THREE (3) VERTICES" << std::endl;
				std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl << std::endl;
				std::cout << "All faces must be triangels. You may have to triangulate the mesh before you try converting it or try again." << std::endl ;
			
				std::cout << "Continue anyway? (y/n)" << std::endl;
				std::cin >> trash;
				std::cout << std::endl << std::endl;

				if(!(strcmp(trash.c_str(), "y") == 0))
				{
					return false;
				}
			}
		}
		continueRead = true;

		//foreach index
		for(unsigned int j = 0; j < face.mNumIndices; j++)
		{
			Vertex vert;

			vert.ID = face.mIndices[j];
			vert.pos = mesh->mVertices[vert.ID];
			vert.uv = mesh->mTextureCoords[0][vert.ID];
			vert.normal = mesh->mNormals[vert.ID];
			vert.tangent = tangents[vert.ID];

			vertexArray.push_back(vert);
		}
	}

	head.numVertices = vertexArray.size();
	head.numTriangles = mesh->mNumFaces;

	return true;
}

//Gets the indexed mesh from Assimp
bool ColladaConverter::GetIndexedMesh(std::vector<Vertex>& vertexArray, std::vector<Face>& faceArray, aiMesh* mesh, Header& head)
{
	std::string trash;
	unsigned int numVerts = 0;
	bool hasTangents = mesh->HasTangentsAndBitangents();

	aiVector3D* binormals;
	aiVector3D* tangents;

	if(hasTangents)
	{
		binormals = mesh->mBitangents;
		tangents = mesh->mTangents;
	}
	else
	{
		std::cout << "The mesh DON'T have binormals and tangents. Tangents and binormals will be calculated instead....or not now anylonger :P" << std::endl << std::endl;
	}


	bool continueRead = false;
	for(unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		Face faceIndices;
		const aiFace& face = mesh->mFaces[i];
		
		if(continueRead == false)
		{
			if(face.mNumIndices > 3)
			{
				std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
				std::cout << "THIS FACE HAS MORE THAN THREE (3) VERTICES" << std::endl;
				std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl << std::endl;
				std::cout << "All faces must be triangels. You may have to triangulate the mesh before converting it or try again." << std::endl ;
			
				std::cout << "Continue anyway? (y/n)" << std::endl;
				std::cin >> trash;
				std::cout << std::endl << std::endl;

				if(!(strcmp(trash.c_str(), "y") == 0))
				{
					return false;
				}
			}
			
		}
		continueRead = true;


		faceIndices.index[0] = face.mIndices[0];
		faceIndices.index[1] = face.mIndices[1];
		faceIndices.index[2] = face.mIndices[2];

		faceArray.push_back(faceIndices);
	}

	for(unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vert;
		vert.ID = i;
		vert.pos = mesh->mVertices[vert.ID];
		vert.uv = mesh->mTextureCoords[0][vert.ID];
		vert.normal = mesh->mNormals[vert.ID];
		vert.tangent = tangents[i];

		vertexArray.push_back(vert);
		numVerts++;
	}

	head.numVertices = vertexArray.size();
	head.numTriangles = faceArray.size();
	head.numIndices = faceArray.size() * 3;

	return true;
}

//Sets the rest of the weights and bone IDs of the vertices
bool ColladaConverter::SetZeroWeights(std::vector<Vertex>& vertexArray)
{
	//If a vertex has less than four weights, set the remaining to zero.
	for(unsigned int i = 0; i < vertexArray.size(); i++)
	{
		if(vertexArray[i].bones.size() < 4)
		{
			for(unsigned int k = vertexArray[i].bones.size(); k < 4; k++)
			{
				vertexArray[i].weights.push_back(0.0f);
				vertexArray[i].bones.push_back(0);
			}
		}
	}
	return true;
}

aiMatrix4x4 ColladaConverter::GetWorldMatrix(aiNode* node)
{
	if(node)
	{
		aiNode* parentBone = node->mParent;
		aiMatrix4x4 matrix = node->mTransformation;
		matrix = GetWorldMatrix(parentBone) * matrix;
		return matrix;
	}

	aiMatrix4x4 matrix;
	return matrix;
}

//Gets the hierarchy of the bones
bool ColladaConverter::GetBoneHierarchy(std::vector<Bone>& boneArray, aiNode* boneNode, int parentID, int &id)
{
	if(!boneNode->mNumMeshes)
	{
		Bone bone;
		bone.name = boneNode->mName;
		bone.nameSize = bone.name.length + 1;
	
		//-------------TEST-----MIGHT BE NEEDED-------------//
		aiMatrix4x4 matrix;
		bone.offsetMatrix = matrix;


		bone.id = id;
		bone.parentID = parentID;
		id++;

		boneArray.push_back(bone);

		for(unsigned int i = 0; i < boneNode->mNumChildren; i++)
		{
			GetBoneHierarchy(boneArray, boneNode->mChildren[i], bone.id, id);
		}	
	}

	return true;
}

//Gets all the bone data
bool ColladaConverter::GetBones(std::vector<Bone>& boneArray, aiNode* rootBone, std::vector<Vertex>& vertexArray, aiMesh* mesh, Header& head)
{
	int id = 0;
	int parentID = -1;
	GetBoneHierarchy(boneArray, rootBone, parentID, id);



	unsigned int numBones = mesh->mNumBones;
	head.numBones = boneArray.size();
	aiBone** bones = mesh->mBones;
	
	for( unsigned int i = 0; i < boneArray.size(); i++ )
	{
		for( unsigned int k = 0; k < numBones; k++)
		{
			if(bones[k]->mName == boneArray[i].name)
			{
				boneArray[i].offsetMatrix = bones[k]->mOffsetMatrix;

				aiVertexWeight* weights = bones[k]->mWeights;

				//Put the weigths into the vertices.
				for(unsigned int l = 0; l < vertexArray.size(); l++)
				{
					for(unsigned int m = 0; m < bones[k]->mNumWeights; m++)
					{
						if(vertexArray[l].ID == weights[m].mVertexId)
						{
							vertexArray[l].weights.push_back(weights[m].mWeight);
							vertexArray[l].bones.push_back(boneArray[i].id);
						}
					}
				}
			}
		}
	}

	for(unsigned int i = 0; i < vertexArray.size(); i++)
	{
		if(vertexArray[i].weights.size() > 4)
		{
			std::cout << "More then 4 weights!  " << vertexArray[i].weights.size() << std::endl;
		}
	}

	SetZeroWeights(vertexArray);
	return true;
}

aiVector3D ColladaConverter::GetWorldTransformationAnimationPosition(std::vector<Bone> boneArray, int boneIndex, unsigned int animIndex, unsigned int framIndex)
{
	if(boneIndex < 0)
	{
		return aiVector3D(0.0f, 0.0f, 0.0f);
	}
	
	if(!boneArray[boneIndex].anim[animIndex].frame.size())
	{
		aiVector3D pos = aiVector3D(0.0f, 0.0f, 0.0f);
		aiVector3D parentPos = GetWorldTransformationAnimationPosition(boneArray, boneArray[boneIndex].parentID, animIndex, framIndex);
		pos.x += parentPos.x;
		pos.y += parentPos.y;
		pos.z += parentPos.z;

		return pos;
	}

	aiVector3D pos = boneArray[boneIndex].anim[animIndex].frame[framIndex].pos;
	aiVector3D parentPos = GetWorldTransformationAnimationPosition(boneArray, boneArray[boneIndex].parentID, animIndex, framIndex);
	pos.x += parentPos.x;
	pos.y += parentPos.y;
	pos.z += parentPos.z;


	return pos;
}


aiVector3D ColladaConverter::GetWorldTransformationAnimationScale(std::vector<Bone> boneArray, int boneIndex, unsigned int animIndex, unsigned int framIndex)
{
	if(boneIndex < 0)
	{
		return aiVector3D(1.0f, 1.0f, 1.0f);
	}
	
	if(!boneArray[boneIndex].anim[animIndex].frame.size())
	{
		aiVector3D scale = aiVector3D(1.0f, 1.0f, 1.0f);
		aiVector3D parentScale = GetWorldTransformationAnimationScale(boneArray, boneArray[boneIndex].parentID, animIndex, framIndex);
		scale.x *= parentScale.x;
		scale.y *= parentScale.y;
		scale.z *= parentScale.z;

		return scale;
	}

	aiVector3D scale = boneArray[boneIndex].anim[animIndex].frame[framIndex].scale;
	aiVector3D parentScale = GetWorldTransformationAnimationScale(boneArray, boneArray[boneIndex].parentID, animIndex, framIndex);
	scale.x *= parentScale.x;
	scale.y *= parentScale.y;
	scale.z *= parentScale.z;

	return scale;
}


aiQuaternion ColladaConverter::GetWorldTransformationAnimationRotation(std::vector<Bone> boneArray, int boneIndex, unsigned int animIndex, unsigned int framIndex)
{
	if(boneIndex < 0)
	{
		return aiQuaternion(0.0f, 0.0f, 0.0f, 1.0f);
	}

	if(!boneArray[boneIndex].anim[animIndex].frame.size())
	{
		aiQuaternion quat = aiQuaternion(0.0f, 0.0f, 0.0f, 1.0f);
		aiQuaternion parentQuat = GetWorldTransformationAnimationRotation(boneArray, boneArray[boneIndex].parentID, animIndex, framIndex);
	
		return parentQuat * quat;
	}

	aiQuaternion quat = boneArray[boneIndex].anim[animIndex].frame[framIndex].quat;
	aiQuaternion parentQuat = GetWorldTransformationAnimationRotation(boneArray, boneArray[boneIndex].parentID, animIndex, framIndex);
	

	return parentQuat * quat;
}

//Gets the animations of the bones
bool ColladaConverter::GetBoneAnimation(std::vector<Bone>& boneArray, aiAnimation** animations, unsigned int numAnimations, std::string AnimationName)
{
	for( unsigned int i = 0; i < numAnimations; i++)
	{
		aiNodeAnim** boneChannels = animations[i]->mChannels;

		for( unsigned int l = 0; l < boneArray.size(); l++)
		{
			BoneAnimation boneAnim;
			boneAnim.name = std::string(AnimationName);
			for(unsigned int k = 0; k < animations[i]->mNumChannels; k++)
			{
				if(boneArray[l].name == boneChannels[k]->mNodeName)
				{

					for (unsigned int m = 0; m < boneChannels[k]->mNumPositionKeys; m++)
					{
						Frame key;
						aiVectorKey pos_key = boneChannels[k]->mPositionKeys[m];
						key.pos = pos_key.mValue;

						aiQuatKey rot_key = boneChannels[k]->mRotationKeys[m];
						key.quat = rot_key.mValue;

						aiVectorKey scale_key = boneChannels[k]->mScalingKeys[m];
						key.scale = scale_key.mValue;
					
						key.time = (float)pos_key.mTime;
						boneAnim.frame.push_back(key);
					}
				}
			}
			boneArray[l].anim.push_back(boneAnim);
		}
	}
	
	return true;
}

//Exports the data to an binary file
bool ColladaConverter::exportFileBinary(std::vector <Vertex> vertexArray, std::vector<Face>& faceArray, Header head, std::vector <Bone> boneArray, unsigned int numAffectingBones, std::string path = "C:/Temp/flag")
{
	std::fstream file ((path + ".bgnome").c_str(), std::ios::out | std::ios::binary);
	if (!file)
	{
		return false;
	}

	/*----------MAGIC---------*/
	char magicByte[] = "GNOME";
	file.write(magicByte, 6);

	/*---------HEADER---------*/
	file.write((char*)&head, (sizeof(head) - sizeof(unsigned int)));
	
	/*---------VERTICES---------*/
	for (unsigned int i = 0; i < vertexArray.size(); i++)
	{
		//Position
		file.write((char*)&vertexArray[i].pos, sizeof(float) * 3);
		float tmp = 1.0f;
		file.write((char*)&tmp, sizeof(float));
		
		//Normal
		file.write((char*)&vertexArray[i].normal, sizeof(float) * 3);
		tmp = 0.0f;
		file.write((char*)&tmp, sizeof(float));

		//Tangent
		file.write((char*)&vertexArray[i].tangent, sizeof(float) * 3);
		file.write((char*)&tmp, sizeof(float));

		//Bone indices
		file.write((char*)vertexArray[i].bones.data(), sizeof(int) * 4);

		//Bone weights
 		file.write((char*)vertexArray[i].weights.data(), sizeof(float) * 4);

		//UV
		file.write((char*)&vertexArray[i].uv.x, sizeof(float));
		file.write((char*)&vertexArray[i].uv.y, sizeof(float));
	}

	/*---------INDICES---------*/
	if(faceArray.size())
	{
		file.write((char*)faceArray.data(), (sizeof(Face) * faceArray.size()));
	}
	

	//If there are bones, export them
	if (numAffectingBones)
	{
		/*---------BONES---------*/
		for (unsigned int i = 0; i < boneArray.size(); i++)
		{
			file.write((char*)&boneArray[i], sizeof(int) * 19);
			file.write(boneArray[i].name.C_Str(), boneArray[i].nameSize);
		}


		if(head.numAnimations)
		{
			//The animationdata is written to another file
			std::fstream animFile ((path + ".bagnome").c_str(), std::ios::out | std::ios::binary);

			/*----------MAGIC---------*/
			char magicAnimByte[] = "ANIMGNOME";
			animFile.write(magicAnimByte, 10);

			animFile.write((char*)&head.numBones, sizeof(unsigned int));
			animFile.write((char*)&head.numAnimations, sizeof(unsigned int));


			/*---------ANIMATIONCLIPS---------*/
			for (unsigned int aminIndex = 0; aminIndex < head.numAnimations; aminIndex++)
			{
				bool writeAnimName = true;

				for (unsigned int i = 0; i < boneArray.size(); i++)
				{
					if(writeAnimName)
					{
						//Animation name
						std::string tmpName = std::string(boneArray[i].anim[aminIndex].name);
						unsigned int nameSize = tmpName.size() + 1;

						animFile.write((char*)&nameSize, sizeof(unsigned int));
						animFile.write(tmpName.c_str(), nameSize);

						writeAnimName = false;
					}
					
					//Number of keyframes
					unsigned int keyFrames = boneArray[i].anim[aminIndex].frame.size();
					animFile.write((char*)&keyFrames, sizeof(unsigned int));

					for (unsigned int j = 0; j < boneArray[i].anim[aminIndex].frame.size(); j++)
					{
						//Keyframe
						animFile.write((char*)&boneArray[i].anim[aminIndex].frame[j], sizeof(Frame));
					}
				}
			}
			animFile.close();
		}
	}
	file.close();

	return true;
}


//Exports the data to an ASCII-file
bool ColladaConverter::exportFileAscii(std::vector <Vertex> vertexArray, std::vector<Face>& faceArray, Header head, std::vector <Bone> boneArray, unsigned int numAffectingBones, std::string path = "C:/Temp/flag")
{
	//E:/Tomte/Flamingo/flamingo.mgnome
	std::fstream file ((path + ".gnome").c_str(), std::ios::out);

	if (!file)
	{
		return false;
	}

	file << "*****HEADER*****" << std::endl;
	file << "#V " << head.numVertices << std::endl;
	file << "#T " << head.numTriangles << std::endl;
	file << "#I " << (head.numTriangles * 3) << std::endl;
	file << "#B " << head.numBones << std::endl;
	file << "#A " << head.numAnimations << std::endl << std::endl;
	

	
	file << "*****VERTICES*****" << std::endl;

	for (unsigned int i = 0; i < vertexArray.size(); i++)
	{
		file << "P " << vertexArray[i].pos.x << " " << vertexArray[i].pos.y << " " << vertexArray[i].pos.z << std::endl;
		file << "N " << vertexArray[i].normal.x << " " << vertexArray[i].normal.y << " " << vertexArray[i].normal.z << " " << std::endl;
		file << "T " << vertexArray[i].tangent.x << " " << vertexArray[i].tangent.y << " " << vertexArray[i].tangent.z << " " << std::endl;
		file << "BN 0 0 0" << std::endl;
		file << "UV " << vertexArray[i].uv.x << " " << vertexArray[i].uv.y <<  std::endl;
		
		if(head.numBones)
		{
			file << "BW";
			for (unsigned int k = 0; k < vertexArray[i].weights.size(); k++)
			{
				file << " " << vertexArray[i].weights[k];
			}
			file << std::endl;
		
			file << "BI";
			for (unsigned int k = 0; k < vertexArray[i].bones.size(); k++)
			{
				file << " " << vertexArray[i].bones[k];
			}
		}
		file << std::endl << std::endl;
	}

	if(faceArray.size())
	{
		file << "*****INDICES*****" << std::endl;
		for (unsigned int i = 0; i < faceArray.size(); i++)
		{
			file << faceArray[i].index[0] << std::endl;
			file << faceArray[i].index[1] << std::endl;
			file << faceArray[i].index[2] << std::endl;
		}
		file << std::endl;
	}

	if (numAffectingBones)
	{
		file << "*****BONEOFFSETS*****" << std::endl;
			
		for (unsigned int i = 0; i < boneArray.size(); i++)
		{
			file <<"jointOffset"<< boneArray[i].id << " ";
		
			for (int j = 0; j < 4; j++)
			{
				for (int k = 0; k < 4; k++)
				{
					file << boneArray[i].offsetMatrix[k][j] << " ";
				}
			}
			file << std::endl;
		}
		file << std::endl;

		file << "*****BONEHIERARCHY*****" << std::endl;
		for (unsigned int i = 0; i < boneArray.size(); i++)
		{
			file << "jointParent"<< boneArray[i].id << " " << boneArray[i].parentID << std::endl;
		}
		file << std::endl << std::endl;
		
		if(head.numAnimations)
		{
			file << "******ANIMATIONCLIPS*******" << std::endl;
			unsigned int aminIndex = 0;
			for (unsigned int aminIndex = 0; aminIndex < head.numAnimations; aminIndex++)
			{
				file << "AnimationClip Take" << aminIndex << std::endl;		
				file << "{" << std::endl;
				for (unsigned int i = 0; i < boneArray.size(); i++)
				{
					if(boneArray[i].anim.size())
					{
						if(aminIndex < boneArray[i].anim.size())
						{
							file << "	joint"<< boneArray[i].id << " #Keyframes: " << boneArray[i].anim[aminIndex].frame.size() << std::endl;
							file << "	{" << std::endl;
							for (unsigned int j = 0; j < boneArray[i].anim[aminIndex].frame.size(); j++)
							{
								file << "		Time: " << boneArray[i].anim[aminIndex].frame[j].time
									<< " Pos: " <<
									boneArray[i].anim[aminIndex].frame[j].pos.x << " " <<
									boneArray[i].anim[aminIndex].frame[j].pos.y << " " <<
									boneArray[i].anim[aminIndex].frame[j].pos.z
									<< " Scale: " <<
									boneArray[i].anim[aminIndex].frame[j].scale.x << " " <<
									boneArray[i].anim[aminIndex].frame[j].scale.y << " " <<
									boneArray[i].anim[aminIndex].frame[j].scale.z
									<< " Quat: " <<
									boneArray[i].anim[aminIndex].frame[j].quat.x << " " <<
									boneArray[i].anim[aminIndex].frame[j].quat.y << " " <<
									boneArray[i].anim[aminIndex].frame[j].quat.z << " " <<
									boneArray[i].anim[aminIndex].frame[j].quat.w << std::endl;
							}
							file << "	}" << std::endl;
						}
					}
				}
				file << "}" << std::endl;
			}
			file << std::endl;
		}
	}

	file.close();

	return true;
}

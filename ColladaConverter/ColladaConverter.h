#ifndef COLLADACONVERTER_H_
#define COLLADACONVERTER_H_



#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/matrix4x4.h>
#include <assimp/quaternion.h>
//#include <assimp/cimport.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "DirectoryHandler.h"

#pragma comment(lib, "Assimp.lib")

class ColladaConverter
{
private:

	struct Header
	{
		unsigned int numVertices;
		unsigned int numTriangles;
		unsigned int numIndices;
		unsigned int numBones;
		unsigned int numAnimations;
	};

	struct Face
	{
		unsigned int index[3];
	};

	struct Vertex
	{
		aiVector3D pos;
		aiVector3D normal;
		aiVector3D tangent;
		std::vector <float> weights;
		std::vector <unsigned int> bones;
		aiVector3D uv;

		unsigned int ID;
	};

	struct Frame
	{
		float time;
		aiVector3D pos;
		aiVector3D scale;
		aiQuaternion quat;
	};

	struct BoneAnimation
	{
		std::string name;
		std::vector<Frame> frame;
	};


	struct Bone
	{
		int id;
		int parentID;
		unsigned int nameSize;
		aiMatrix4x4 offsetMatrix;
	
		aiString name;
		std::vector<BoneAnimation> anim;
	};

public:

	ColladaConverter();
	~ColladaConverter();

	int Convert(bool batchConvert, std::string file );
	int Convert(bool batchConvert, std::string file, std::string gFile);
	std::string GetFileNameAndPath(std::string filename, std::string delim);

private:
	bool GetFilesFromMetaFile(std::vector<std::string> &files, std::vector<std::string> &animationNames, const char* filePath);
	bool GetBoneAnimation(std::vector<Bone>& boneArray, aiAnimation** animations, unsigned int numAnimations, std::string AnimationName);

	aiMatrix4x4 GetWorldMatrix(aiNode* node);
	aiVector3D GetWorldTransformationAnimationPosition(std::vector<Bone> boneArray, int boneIndex, unsigned int animIndex, unsigned int framIndex);

	aiVector3D GetWorldTransformationAnimationScale(std::vector<Bone> boneArray, int boneIndex, unsigned int animIndex, unsigned int framIndex);

	aiQuaternion GetWorldTransformationAnimationRotation(std::vector<Bone> boneArray, int boneIndex, unsigned int animIndex, unsigned int framIndex);


	bool GetBones(std::vector<Bone>& boneArray, aiNode* rootNode, std::vector<Vertex>& vertexArray, aiMesh* mesh, Header& head);

	bool GetBoneHierarchy(std::vector<Bone>& boneArray, aiNode* boneNode, int parentID, int &id);


	bool GetBoneData(std::vector<Bone>& boneArray, std::vector<Vertex>& vertexArray, aiMesh* mesh, Header& head);
	bool SetZeroWeights(std::vector<Vertex>& vertexArray);
	bool GetIndexedMesh(std::vector<Vertex>& vertexArray, std::vector<Face>& faceArray, aiMesh* mesh, Header& head);
	bool GetMesh(std::vector<Vertex>& vertexArray, aiMesh* mesh, Header& head);
	void CalculateTangent(
		aiVector3D P0,
		aiVector3D P1,
		aiVector3D P2,
		aiVector3D UV0,
		aiVector3D UV1,
		aiVector3D UV2,
		aiVector3D &tangent,
		aiVector3D &binormal
		);

	bool exportFileAscii(std::vector <Vertex> vertexArray, std::vector<Face>& faceArray, Header head, std::vector <Bone> boneArray, unsigned int numAffectingBones, std::string path);

	bool exportFileBinary(std::vector <Vertex> vertexArray, std::vector<Face>& faceArray, Header head, std::vector <Bone> boneArray, unsigned int numAffectingBones, std::string path);


};
#endif

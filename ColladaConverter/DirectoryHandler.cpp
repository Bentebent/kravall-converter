#include "DirectoryHandler.h"


DirectoryHandler::DirectoryHandler()
{
}

DirectoryHandler::~DirectoryHandler()
{
}

#ifdef _WIN32

void DirectoryHandler::GetFilesInFolder(std::vector<std::string> &files, const char* filePath, unsigned int filePathCharSize)
{
	WIN32_FIND_DATA fd;

	WCHAR* tmpfilePath = new WCHAR[filePathCharSize];

	mbstowcs(tmpfilePath, filePath, filePathCharSize);



    HANDLE h = FindFirstFile(tmpfilePath,&fd);

    if(h == INVALID_HANDLE_VALUE)
    {
		FindClose(h);
		delete [] tmpfilePath;
        return; // no files found
    }

    while(true)
    {
		char wc[260];
		int numBytes = WideCharToMultiByte(
			CP_ACP,
			0,
			fd.cFileName,
			-1,
			(LPSTR)wc,
			260,
			nullptr,
			nullptr
			);


		files.push_back(wc);
		

		if(FindNextFile(h, &fd) == FALSE)
		{
			break;
		}
    }

	FindClose(h);
	delete [] tmpfilePath;
	return;
}

void DirectoryHandler::GetSubFolders(std::vector<std::string> &files, const char* folder, unsigned int folderCharSize)
{
	WIN32_FIND_DATA fd;

	WCHAR* tmpfilePath = new WCHAR[folderCharSize];

	mbstowcs(tmpfilePath, folder, folderCharSize);



    HANDLE h = FindFirstFile(tmpfilePath,&fd);

    if(h == INVALID_HANDLE_VALUE)
    {
		FindClose(h);
		delete [] tmpfilePath;
        return; // no files found
    }

    while(true)
    {
		if(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			char wc[260];
			int numBytes = WideCharToMultiByte(
				CP_ACP,
				0,
				fd.cFileName,
				-1,
				(LPSTR)wc,
				260,
				nullptr,
				nullptr
				);


			files.push_back(wc);
		}

		if(FindNextFile(h, &fd) == FALSE)
		{
			break;
		}
    }

	FindClose(h);
	delete [] tmpfilePath;
	return;
}

#elif __unix__

void DirectoryHandler::GetFilesInFolder(std::vector<std::string> &files, const char* filePath, unsigned int filePathCharSize)
{
	//TODO: add linux specific code here
	return;
}


void DirectoryHandler::GetSubFolders(std::vector<std::string> &files, const char* folder, unsigned int folderCharSize)
{
	//TODO: add linux specific code here


    DIR *dir = opendir(folder);

    struct dirent *entry = readdir(dir);

    while (entry != NULL)
    {
        if (entry->d_type == DT_DIR)
		{
            //printf("%s\n", entry->d_name);
			files.push_back(entry->d_name);
		}
        entry = readdir(dir);
    }

    closedir(dir);

	return;
}

#endif
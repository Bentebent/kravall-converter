#ifndef DIRECTORYHANDLER_H_
#define DIRECTORYHANDLER_H_


#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
//#include <cstdlib>

#elif __unix__
//TODO: add linux includes here
#include <dirent.h>	//is an example, I don't know :P

#endif

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


class DirectoryHandler
{
public:
	DirectoryHandler();
	~DirectoryHandler();

	

	void GetFilesInFolder(std::vector<std::string> &files, const char* filePath, unsigned int filePathCharSize);

	void GetSubFolders(std::vector<std::string> &folders, const char* filePath, unsigned int filePathCharSize);
};
#endif
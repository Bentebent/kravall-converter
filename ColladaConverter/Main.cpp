#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "ColladaConverter.h"

int Batch(ColladaConverter* converter, DirectoryHandler* dir, std::string fileDirectory)
{
	
	std::vector<std::string> tmpTestfiles;
	std::string tmp = std::string(fileDirectory + "/*.mgnome");
	dir->GetFilesInFolder(tmpTestfiles, tmp.c_str(), tmp.size() + 1);
	for(unsigned int i = 0; i < tmpTestfiles.size(); i++)
	{
		int val = converter->Convert(true, std::string(fileDirectory + "/" + tmpTestfiles[i]));

        if( val )
            return val;
	}


	std::vector<std::string> tmpTestfolders;
	tmp = std::string(fileDirectory + "/*");
	dir->GetSubFolders(tmpTestfolders, tmp.c_str(), tmp.size() + 1);


	for(unsigned int i = 2; i < tmpTestfolders.size(); i++)
	{
		Batch(converter, dir, std::string(fileDirectory + "/" + tmpTestfolders[i]));
	}

    return 0;
}


int main(int argc, char** argv)
{
    std::cout << "Starting.." << std::endl;
    std::cout << "Built: " << __TIMESTAMP__ << std::endl;
	DirectoryHandler* dir = new DirectoryHandler();
	ColladaConverter* converter = new ColladaConverter();
	bool batchConvert = false;

	std::string trash;
	std::string filename;
	std::string fileDirectory;

    std::string destination = "";

    if( argc >= 2 )
    {
        filename = argv[1];

        std::cout << "input file:" << filename << std::endl;
        
        if( argc >= 3 )
        { 
            destination = argv[2];
            std::cout << "destination file:" << destination << std::endl;
        }
		
#ifdef WIN32
		std::replace(filename.begin(), filename.end(), '\\', '/');
#endif


        batchConvert = false;
    }
    else
    {
        std::cout << "Batch convert? (y/n)" <<std::endl;
        std::cin >> trash;

        if((strcmp(trash.c_str(), "y") == 0))
        {
            batchConvert = true;
            std::cout << "Start folder for batch converting?" <<std::endl;
            std::cin >> fileDirectory;
        }
        else
        {
            std::cout << "Filepath to file to convert?" <<std::endl;
            std::cin >> filename;
        }
    }

	if(batchConvert)
	{
#ifdef WIN32
		std::replace(fileDirectory.begin(), fileDirectory.end(), '\\', '/');
#endif
		Batch(converter, dir, fileDirectory);
	}
	else
	{
#ifdef WIN32
		std::replace(filename.begin(), filename.end(), '\\', '/');
#endif
		int err = converter->Convert(batchConvert, filename, destination );
        if ( err )
            return err;
	}

	delete converter;
	converter = 0;

	delete dir;
	dir = 0;

	if(batchConvert)
	{
		std::cout << "Batch convert complete" << std::endl;
		trash = std::string("");
		std::cin >> trash;
	}
	return 0;
}

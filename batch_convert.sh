#!/bin/bash

## Kill program if any failure.
set -e

## To force conversion, give first command "force"

UNAME=$(uname)

echo "$UNAME"

if [ "$UNAME" = "Linux" ]; then
    echo "Setting converter to linux"
    CONVERTER=collada_converter
else
    echo "Setting converter to windows"
    CONVERTER=./ColladaConverter.exe
fi


echo "Mirroring folder structure..."
## Mirror folder structure to ensure all necessary folder exist
find ./riot_art/assets/ | awk '{
    n=split($1,arr,"/")
    printf( "./riot/assets" )
    for( i=4; i+1 <=n; i++) {
        printf( "/%s", arr[i] )
    }
    printf( "\n" )
}' | xargs mkdir -pv

echo "Generating default mgnome files..."
## Generate default mgnome files for all models, in static that is missing one.
STATIC_DAE=$(find ./riot_art/assets/model/static/ ./riot_art/assets/model/dev/ -name "*dae" | awk '{
    n = split( $1, arr, "." )
    for(i=1;i<n;i++ ) {
        printf( "%s.", arr[i] )
    }
    printf( "mgnome\n" )
}')

for MGNOME in $STATIC_DAE
do
    if [ ! -f $MGNOME ]; then
        echo "MISSING following mgnome for static model, creating a file"
        echo $MGNOME
        echo $MGNOME | awk '{
        n1 = split( $1, arr1, "/" )
        n2 = split( arr1[n1], arr2, "." )
        
        for( i=1;i<n2;i++ ) {
            printf( "%s.", arr2[i] )
        }
        printf( "dae\n" )
    }' > $MGNOME
    fi
done

echo "Generating default object .lua files..."
## Generate default lua files for all models, in static that is missing one.
STATIC_DAE=$(find ./riot_art/assets/model/static/ ./riot_art/assets/model/dev/ -name "*dae" | awk '{
    n = split( $1, arr, "." )
    for(i=1;i<n;i++ ) {
        printf( "%s.", arr[i] )
    }
    printf( "lua\n" )
}')

for LUA_FILE in $STATIC_DAE
do
    if [ ! -f $LUA_FILE ]; then
        echo "MISSING following lua for static model, creating a file"
        echo $LUA_FILE
        echo $LUA_FILE | awk '{
        n1 = split( $1, arr1, "/" )

        printf( "return {\"" )

        for( i=3;i<n1;i++ ){
            printf( "%s/", arr1[i] ) 
        }
        n2 = split( arr1[n1], arr2, "." )
        
        for( i=1;i<n2;i++ ) {
            printf( "%s.", arr2[i] )
        }
        printf( "bgnome\",{\"assets/texture/dev/default.material\"}}\n" )
    }' > $LUA_FILE
    fi
done

echo "Converting gnome assets..."
## Convert all gnome assets
GNOME_FILES=$(find ./riot_art/assets/ -name "*mgnome")

if [[ $1 == "force" ]]; then
    echo "Force exporting all files."
fi

for MGNOME in $GNOME_FILES
do
    NEEDS_EXPORT=""
    if [[ $1 == "force" ]]; then
        NEEDS_EXPORT="true"
    fi

    #Get the directory path for model
    M_DIR=$(echo "$MGNOME" | awk '{
        n=split($1,arr,"/")
        
        printf( "." )
        for(i=2;i<n;i++) {
            printf( "/%s", arr[i] )
        }

        printf("\n")
    }') 

    #Get the full path for the bgnome file
    BGNOME=$(echo "$MGNOME" | awk '{
        n=split($1,arr,"/");
        printf( "./riot/assets" );
        for(i=4;i<=n;i++) {
            if( i == n ) {
                ln = split( arr[i], last, "." );
                printf( "/" );
                for( k=1; k+1<=ln; k++ ) {
                    printf( "%s", last[k] );
                }
            } else {
                printf( "/%s", arr[i] );
            }
        }
        print ".bgnome"
    }')

    #Generate full path for all daes
    DAE_FILES=$(cat $MGNOME | awk -v dir="$M_DIR" '{
       printf( "%s/%s\n", dir, $1 )  
    }')


    if [[ -e $BGNOME ]]; then
        if [[ $BGNOME -ot $MGNOME ]]; then
            echo "The following .mgnome file has changed:$MGNOME"
            NEEDS_EXPORT="true"
        fi
        
        for DAE in $DAE_FILES
        do
                if [[ $BGNOME -ot $DAE ]]; then
                    echo "Following DAE is newer than .bgnome:$DAE"
                    NEEDS_EXPORT="true"
                fi
        done
    else
        echo "No bgnome found, exporting:$BGNOME"
        NEEDS_EXPORT="true"
    fi

    if [[ -n $NEEDS_EXPORT ]]; then
        echo "File needs exporting:$BGNOME"
        echo "Exporting from:$MGNOME"
        echo "The following files:$DAE_FILES"

        echo $MGNOME | awk '{
        n=split($1,arr,"/");
            printf( "." );
            for(i=2;i<=n;i++) {
                printf( "/%s", arr[i] );
            }

            printf( " ./riot/assets" );

           
            for(i=4;i<=n;i++) {
                if( i == n ) {
                    ln = split( arr[i], last, "." );
                    printf( "/" );
                    for( k=1; k+1<=ln; k++ ) {
                        printf( "%s", last[k] );
                    }
                } else {
                    printf( "/%s", arr[i] );
                }
            }

            print ""

        }' | xargs --max-lines=1 $CONVERTER

        echo ""
    fi 
done

#DAE_FILES=$(cat $MGNOME | awk '{ printf  

echo "Copying all object files, materials and textures..."
##Copy all png images, lua files and material files.
find ./riot_art/assets/ -name "*.png" -or -name "*.lua" -or -name "*.material" | awk '{
    n=split($1,arr,"/")
    printf( "%s ", $1 )
    printf( "./riot/assets" )
    for( i=4; i <=n; i++) {
        printf( "/%s", arr[i] )
    }
    printf( "\n" )
}' | xargs --max-lines=1 cp -f

echo "Generating manifest of all objects..."
##Generate manifest file for all objects.
find ./riot_art/assets/ -name "*.lua" | awk 'BEGIN { 
print( "{" )
}
{
    n=split($1,arr,"/")
    if( NR != 1 ) {
        printf( ",\n" )
    }

    printf( "\"." )
    for( i=4; i <= n; i++) {
        printf( "/%s", arr[i] )
    }

    printf( "\"" )
}
END {
    print( "\n}" )
}' > ./riot/assets/object_manifest.lua

echo "\"You will stop at nothing to reach your objective, but only because your
brakes are defective.\""
echo "Done batch conversion, Everything OK."

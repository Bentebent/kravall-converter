bl_info = {
 "name": "Kravall COLLADA Export",
 "author": "Lukas Orsvärn",
 "version": (1, 0, 1),
 "blender": (2, 69, 0),
 "location": "Ctrl Shift E",
 "description": "Export a COLLADA rotated 90 degrees on the x axis",
 "category": "Import-Export"}

import bpy, mathutils, math

class ExportKravallCollada(bpy.types.Operator):
    """Export as Kravall COLLADA file"""
    bl_idname = "riot.export_collada"
    bl_label = "Export Kravall COLLADA"

    filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    #filename = bpy.props.StringProperty(subtype="FILE_NAME")
    #directory = bpy.props.StringProperty(subtype="DIR_PATH")
    #files = bpy.props.CollectionProperty(type=bpy.types.OperatorFileListElement)

    def invoke(self, context, event):
        bpy.context.window_manager.fileselect_add(self)
        return{'RUNNING_MODAL'}

    def execute(self, context):
        ob = context.active_object
        deg90Rad = 90 * math.pi / 180
        self.filepath = self.filepath.replace("\\", "/")
        if self.filepath.find(".dae") == -1:
            self.filepath += ".dae"

        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
        ob.rotation_euler = mathutils.Euler((-deg90Rad, 0, 0), 'XYZ')
        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
        
        bpy.ops.wm.collada_export( \
            filepath=self.filepath, \
            check_existing=True, \
            filter_blender=False, \
            filter_backup=False, \
            filter_image=False, \
            filter_movie=False, \
            filter_python=False, \
            filter_font=False, \
            filter_sound=False, \
            filter_text=False, \
            filter_btx=False, \
            filter_collada=True, \
            filter_folder=True, \
            filemode=8, \
            display_type='FILE_DEFAULTDISPLAY', \
            apply_modifiers=True, \
            export_mesh_type=0, \
            export_mesh_type_selection='view', \
            selected=True, \
            include_children=False, \
            include_armatures=False, \
            include_shapekeys=False, \
            deform_bones_only=False, \
            active_uv_only=False, \
            include_uv_textures=False, \
            include_material_textures=False, \
            use_texture_copies=False, \
            triangulate=True, \
            use_object_instantiation=False, \
            sort_by_name=False, \
            export_transformation_type=0, \
            export_transformation_type_selection='matrix', \
            open_sim=False
        )
        
        ob.rotation_euler = mathutils.Euler((deg90Rad, 0, 0), 'XYZ')

        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

        f = open(self.filepath, 'r', encoding='utf-8')
        daeFile = f.read()
        f.close()

        f = open(self.filepath, 'w', encoding='utf-8')
        f.write( daeFile.replace("Z_UP", "Y_UP") )
        f.close()
        

        return{'FINISHED'}


addon_keymaps = []

def register():
    bpy.utils.register_module(__name__)

    # Handle the keymap.
    kc = bpy.context.window_manager.keyconfigs.addon
    if kc:
        km = kc.keymaps.new(name="Object Mode", space_type='EMPTY')
        kmi = km.keymap_items.new("riot.export_collada", 'E', 'PRESS', ctrl=True, shift=True, head=True)
        addon_keymaps.append(km)

def unregister():
    bpy.utils.unregister_module(__name__)

    # Handle the keymap.
    wm = bpy.context.window_manager
    for km in addon_keymaps:
        wm.keyconfigs.addon.keymaps.remove(km)

    addon_keymaps.clear()

if __name__ == "__main__":
    register()

solution "RiotGame"
    configurations {"Debug", "Release" }
        flags{ "Unicode", "NoPCH" } 
        libdirs { }
        includedirs { }
		--startproject "core"

    configuration { "Debug" }
        defines { "DEBUG" }
        flags { "Symbols" }
    
    local location_path = "build"

    if os.is( "linux" ) then
        buildoptions { "-Wall -std=c++11"}
    elseif(os.is("windows")) then
    end
    
	defines { "_CRT_SECURE_NO_WARNINGS" }

    project "ColladaConverter"
        targetname "collada_converter"
        debugdir ""
        --location ( location_path )
        language "C++"
        kind "ConsoleApp"
        files { "ColladaConverter/**.cpp", "ColladaConverter/**.hpp" }

        links { "assimp" }
    
